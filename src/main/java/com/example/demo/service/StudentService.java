package com.example.demo.service;

import java.util.ArrayList;

import com.example.demo.model.Student;

public interface StudentService {
	public ArrayList<Student> findAll();
	public Student getStudentByName(String name);
}
