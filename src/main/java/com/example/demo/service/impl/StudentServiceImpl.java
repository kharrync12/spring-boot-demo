package com.example.demo.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;

import com.example.demo.model.Student;
import com.example.demo.service.StudentService;
import com.opencsv.CSVReader;

@Service
public class StudentServiceImpl implements StudentService {
	private static final Logger logger = Logger.getLogger(StudentServiceImpl.class.getName());
	
	private ArrayList<Student> students = null;
	
	private void readCsv() {
		students = new ArrayList<>();
		FileInputStream fis = null;
		CSVReader reader = null;
        try {
            String fileName = "src/main/resources/students.csv";

            //build reader instance
            //ready csv file
            //default separator is comma
            //default quote character is double quote
            //start reading from line 2
            fis = new FileInputStream(new File(fileName));
            reader = new CSVReader(new InputStreamReader(fis), ',', '"', 1);
            String[] nextLine;
            
            while ((nextLine = reader.readNext()) != null) {
            	if(nextLine != null) {
            		Student newStudent = new Student(nextLine[0], nextLine[1]);
            		students.add(newStudent);
            	}
            }
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
        	logger.log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
            reader = null;
        }
	}
	
	@Override
	public ArrayList<Student> findAll() {
		readCsv();
        return students;
	}

	@Override
	public Student getStudentByName(String name) {
		readCsv();
		for(Student s : students) {
			if(name.equals(s.getName())) {
				return s;
			}
		}
		return null;
	}
}
