package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Student;
import com.example.demo.service.StudentService;

@RestController
public class SampleRestController {

	@Autowired
	public StudentService studentService;
	
	@RequestMapping("/")
	public String welcome() {
		return "Welcome to Spring Demo App";
	}
	
	@RequestMapping("/students/list")
	public List<Student> listStudents() {
		return studentService.findAll();
	}
	
	@RequestMapping("students/{name}")
	public Student getStudent(@PathVariable String name) {
		return studentService.getStudentByName(name);
	}
}	 
